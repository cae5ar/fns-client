package com.nms.fns.client;

import org.apache.log4j.Logger;
import org.junit.Test;

import java.util.List;


public class AppTest {

    private final Logger logger = Logger.getLogger(AppTest.class);

    @Test
    public void oracleConnectionTest() throws Exception {
        OracleConnection oracleConnection = new OracleConnection(App.jdbcUrl);
        boolean result = oracleConnection.execute(App.startEgripJobQuery);
        assert result;
    }

    @Test
    public void fnsClientEgripTest() throws Exception {
        List<String> dirs = new DirLnksParser(
                App.egripFolderUrl,
                new UrlFinder()
        ).getDirs(new FnsClientImpl(App.host, new SSLContextBuilderImpl(App.egripCertificateName, App.egripCertificatePassword)));
        logger.info(dirs);
        assert !dirs.isEmpty();
    }
    @Test
    public void fnsClientEgrulTest() throws Exception {
        List<String> dirs = new DirLnksParser(
                App.egrulFoldeUrl,
                new UrlFinder()
        ).getDirs(new FnsClientImpl(App.host, new SSLContextBuilderImpl(App.egrulCertificateName, App.egrulCertificatePassword)));
        logger.info(dirs);
        assert !dirs.isEmpty();
    }
}
