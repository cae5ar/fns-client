package com.nms.fns.client;

import com.nms.fns.client.checker.Checker;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

class UrlFinder {

    private final Checker checker;
    private Logger logger = Logger.getLogger(UrlFinder.class);

    public UrlFinder() {
        this(null);
    }
    public UrlFinder(Checker checker) {
        this.checker = checker;
    }

    public List<String> parseUrls(InputStream inputStream) throws IOException {
        List<String> urls = new ArrayList<>();
        Document parsedDocument = Jsoup.parse(inputStream, "utf-8", "");
        Elements anchors = parsedDocument.select("#directory-listing>li[data-href]>a");
        Stream<Element> anchorsStream = anchors.stream();
        if(checker != null)
            anchorsStream = anchorsStream.filter(checker::check);
        anchorsStream.map(a->a.attr("href")).forEach(urls::add);
        return urls;
    }
}
