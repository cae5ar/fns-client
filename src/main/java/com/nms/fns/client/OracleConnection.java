package com.nms.fns.client;

import org.apache.log4j.Logger;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

class OracleConnection {

    private final Logger logger = Logger.getLogger(OracleConnection.class);
    private final Connection conn;

    public OracleConnection(String jdbcUrl) {
        try {
            Class.forName("oracle.jdbc.OracleDriver");
            conn = DriverManager.getConnection(jdbcUrl);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public boolean execute(String execute) {
        try {
            logger.info("Execute: " + execute);
            CallableStatement callableStatement = conn.prepareCall("{" + execute + "}");
            return callableStatement.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
