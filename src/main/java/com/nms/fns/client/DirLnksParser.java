package com.nms.fns.client;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

class DirLnksParser {

    private final String egripFolderUri;
    private final UrlFinder urlFinder;
    private Logger logger = Logger.getLogger(DirLnksParser.class);

    DirLnksParser(String egripFolder, UrlFinder urlFinder) {
        this.egripFolderUri = egripFolder;
        this.urlFinder = urlFinder;

    }

    List<String> getDirs(FnsClient fnsClient) {
        try (InputStream inputStream = fnsClient.retrieve(egripFolderUri)) {
            try {
                return urlFinder.parseUrls(inputStream);
            }
            catch (IOException e) {
                throw new RuntimeException("EGRIP folder hasn't been parsed");
            }
        }
        catch (IOException e) {
            throw new RuntimeException("EGRIP folder can't be fetched");
        }
    }
}
