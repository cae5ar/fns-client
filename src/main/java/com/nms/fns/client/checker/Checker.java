package com.nms.fns.client.checker;

import org.jsoup.nodes.Element;

public interface Checker {
    boolean check(Element value);
}
