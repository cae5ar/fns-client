package com.nms.fns.client.checker;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Element;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ModifyDateMoreThanCheker implements Checker {

    private final Logger loger = Logger.getLogger(ModifyDateMoreThanCheker.class);
    private final Checker anotherChecker;
    private final Date date;
    private final DateFormat dateFormat;

    public ModifyDateMoreThanCheker(String date, String datePattern, Checker anotherChecker) {
        this.anotherChecker = anotherChecker;
        this.dateFormat = new SimpleDateFormat(datePattern);
        try {
            this.date = this.dateFormat.parse(date);
        }
        catch (ParseException e) {
            throw new RuntimeException("Wrong date as condition");
        }

    }

    @Override
    public boolean check(Element element) {
        if (anotherChecker == null || anotherChecker.check(element)) {
            try{
                String content = element.select(".file-modified").first().text().trim();
                Date parsedDate = dateFormat.parse(content);
                loger.trace("Compare " + content + " and " + dateFormat.format(date));
                return date.before(parsedDate);
            }catch (Exception e){
                loger.error(e);
                return false;
            }
        }
        else
            return false;
    }
}
