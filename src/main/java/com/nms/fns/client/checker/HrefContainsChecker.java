package com.nms.fns.client.checker;

import org.jsoup.nodes.Element;

public class HrefContainsChecker implements Checker{

    private final String substring;
    private final boolean contains;
    private final Checker anotherChecker;

    public HrefContainsChecker(String substring) {
        this(substring, true, null);
    }

    public HrefContainsChecker(String substring, Checker anotherChecker) {
        this(substring, true, anotherChecker);
    }

    public HrefContainsChecker(String substring, boolean contains) {
        this(substring, contains, null);
    }

    public HrefContainsChecker(String substring, boolean contains, Checker anotherChecker) {
        this.substring = substring;
        this.contains = contains;
        this.anotherChecker = anotherChecker;
    }

    @Override
    public boolean check(Element element) {
        if (anotherChecker == null || anotherChecker.check(element)) {
            String linkHref = element.attr("href");
            return contains == linkHref.contains(substring);
        }else
            return false;
    }
}
