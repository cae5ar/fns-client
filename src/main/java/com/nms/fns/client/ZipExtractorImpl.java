package com.nms.fns.client;

import org.apache.log4j.Logger;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipExtractorImpl implements ZipExtractor {

    private final Path extractDirectory;
    private final Logger logger = Logger.getLogger(ZipExtractorImpl.class);
    private final int BUFFER_SIZE = 4096;

    ZipExtractorImpl(Path extractDirectory) {
        this.extractDirectory = extractDirectory;
        if(!extractDirectory.toFile().exists()){
            extractDirectory.toFile().mkdirs();
            extractDirectory.toFile().setReadable(true, false);
            extractDirectory.toFile().setWritable(true, false);
            extractDirectory.toFile().setExecutable(true, false);
        }
    }

    public void extract(Path file) {
        try {
            logger.info("START EXTRACT " + file);
            try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(file.toString()))) {
                ZipEntry entry = zipInputStream.getNextEntry();
                while (entry != null) {
                    Path unzippedFilename = FileSystems.getDefault().getPath(extractDirectory.toString(), entry.getName());
                    if (!entry.isDirectory()) {
                        extractFile(zipInputStream, unzippedFilename);
                    }
                    else {
                        unzippedFilename.toFile().mkdir();
                    }
                    zipInputStream.closeEntry();
                    entry = zipInputStream.getNextEntry();
                }
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        logger.info("END EXTRACT " + file);
    }

    private void extractFile(ZipInputStream zipIn, Path filePath) throws IOException {
        File file = filePath.toFile();
        file.setReadable(true, false);
        file.setWritable(true, false);
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file))) {
            byte[] bytesIn = new byte[BUFFER_SIZE];
            int read = 0;
            while ((read = zipIn.read(bytesIn)) != -1) {
                bos.write(bytesIn, 0, read);
            }
        }
    }

    public ZipExtractor clone() {
        return new ZipExtractorImpl(this.extractDirectory);
    }

}
