package com.nms.fns.client;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Semaphore;

public class FileDownloadManager implements Runnable{
    private final String threadName;
    private final String downloadDirectory;
    private final ZipExtractor prototype;
    private final FnsClient fnsClient;
    private final ZipLinksParser zipLinksParser;
    private final List<Thread> downloaderThreads = new ArrayList<>();
    private final Logger logger = Logger.getLogger(FileDownloader.class);

    private final Semaphore semaphore = new Semaphore(3, true);

    FileDownloadManager(String threadName, String downloadDirectory, ZipExtractor prototype, FnsClient fnsClient,
            ZipLinksParser zipLinksParser) {
        this.threadName = threadName;
        this.downloadDirectory = downloadDirectory;
        this.prototype = prototype;
        this.fnsClient = fnsClient;
        this.zipLinksParser = zipLinksParser;
    }

    @Override
    public void run() {
        Thread.currentThread().setName(threadName);
        logger.info("execute");
        Map<String, List<String>> stringListMap = zipLinksParser.parseZipLinks(fnsClient);
        stringListMap.values().stream().flatMap(List::stream).forEach(s-> downloaderThreads.add(this.downloadFile(s)));
        downloaderThreads.forEach(t->{
            try {
                t.join();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        logger.info("complete");
    }

    private Thread downloadFile(String url) {
        String[] splited = url.split("/");
        String fileName = splited[splited.length - 1];
        String fileFolderName = splited[splited.length - 2];
        Path fileFolder = FileSystems.getDefault().getPath(downloadDirectory, fileFolderName);
        Thread thread = new Thread(
                new FileDownloader(
                        semaphore,
                        fnsClient.clone(),
                        url,
                        fileName,
                        fileFolder,
                        prototype.clone()
                ),
                threadName+"_"+fileFolderName
        );
        thread.start();
        return thread;
    }

    private static class FileDownloader implements Runnable {

        private static final String DOWNLOAD_SUFFIX = ".download";
        private final Semaphore semaphore;
        private final String url;
        private final String filename;
        private final FnsClient fnsClient;
        private final Path fileFolder;
        private final ZipExtractor extractor;

        private final Logger logger = Logger.getLogger(FileDownloader.class);

        public FileDownloader(Semaphore semaphore, FnsClient fnsClient, String url, String filename, Path fileFolder, ZipExtractor extractor) {
            this.semaphore = semaphore;
            this.url = url;
            this.fileFolder = fileFolder;
            this.extractor = extractor;
            this.filename = filename;
            this.fnsClient = fnsClient;
        }

        @Override
        public void run() {
            try {
                semaphore.acquire();
                Path targetPath = FileSystems.getDefault().getPath(fileFolder.toAbsolutePath().toString(), filename);
                if(targetPath.toFile().exists()) {
                    logger.warn("zip already downloaded, skip this item " + targetPath);
                }
                else{
                    fileFolder.toFile().mkdirs();
                    logger.info("START DOWNLOAD " + targetPath);
                    Path downloadPath = FileSystems.getDefault().getPath(targetPath.toString()+ DOWNLOAD_SUFFIX);
                    try (InputStream inputStream = fnsClient.retrieve(url)) {
                        logger.debug("Temp download file name is " + downloadPath);
                        if(downloadPath.toFile().exists()) {
                            logger.warn("Temp download file name already exists. It will be overridden " + downloadPath);
                            downloadPath.toFile().delete();
                        }
                        Files.copy(inputStream, downloadPath);
                    }
                    logger.debug("rename downloaded file to " + targetPath);
                    downloadPath.toFile().renameTo(targetPath.toFile());
                    logger.info("SUCCESS DOWNLOAD " + targetPath);
                    extractor.extract(targetPath);
                }
            }
            catch (InterruptedException | IOException e) {
                e.printStackTrace();
            }
            finally {
                semaphore.release();
            }
        }
    }
}
