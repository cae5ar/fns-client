package com.nms.fns.client;

import java.nio.file.Path;

public interface ZipExtractor {

    void extract(Path file);

    ZipExtractor clone();
}
