package com.nms.fns.client;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

public class SSLContextBuilderImpl implements SSLContextBuilder {

    private final SSLContext sslContext;
    private final String certificateName;

    SSLContextBuilderImpl(String certificateName, String password) {
        this.certificateName = certificateName;
        try {
            InputStream certResource = this.getClass().getClassLoader().getResourceAsStream(certificateName);
            KeyStore clientStore = KeyStore.getInstance("PKCS12");
            clientStore.load(certResource, password.toCharArray());

            KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(clientStore, password.toCharArray());
            KeyManager[] kms = kmf.getKeyManagers();

            TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };

            sslContext = SSLContext.getInstance("SSL");
            sslContext.init(kms, trustAllCerts, new SecureRandom());
        }
        catch (Exception e) {
            throw new RuntimeException("Certificate can't be initialized",e);
        }
    }

    @Override
    public SSLContext get() {
        return sslContext;
    }

    @Override public String toString() {
        return "SSLContextBuilderImpl{" + "certificateName=" + certificateName + ", sslContext='" + sslContext + '\''
                + '}';
    }
}
