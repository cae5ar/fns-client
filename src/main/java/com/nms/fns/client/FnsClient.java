package com.nms.fns.client;

import java.io.InputStream;

public interface FnsClient {

    InputStream retrieve(String egripFolderUri);
    FnsClient clone();
}
