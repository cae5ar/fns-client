package com.nms.fns.client;

import com.nms.fns.client.checker.HrefContainsChecker;
import com.nms.fns.client.checker.ModifyDateMoreThanCheker;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystems;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;

public class App {

    static String host;
    static String baseDirectory;

    static String egripCertificateName;
    static String egripCertificatePassword;
    static String egripDownloadDirectory;
    static String egripExtractDirectory;
    static String egripFolderUrl;

    static String egrulCertificateName;
    static String egrulCertificatePassword;
    static String egrulDownloadDirectory;
    static String egrulExtractDirectory;
    static String egrulFoldeUrl;

    static String jdbcUrl;
    static String startEgripJobQuery;
    static String startEgrulJobQuery;
    static String datePattern;

    private static final Logger logger = Logger.getLogger(App.class);

    static {
        Properties properties = new Properties();
        try(InputStream resourceAsStream = App.class.getClassLoader().getResourceAsStream("app.properties")){
            properties.load(resourceAsStream);
            logger.trace(properties);
            logger.trace("start app configuration");

            host = properties.getProperty("host");
            baseDirectory = properties.getProperty("baseDirectory");
            egripCertificateName = properties.getProperty("egripCertificateName");
            egripCertificatePassword = properties.getProperty("egripCertificatePassword");
            egripDownloadDirectory = baseDirectory + properties.getProperty("egripDownloadDirectory");
            egripExtractDirectory = baseDirectory + properties.getProperty("egripExtractDirectory");
            egripFolderUrl = properties.getProperty("egripFolderUrl");
            egrulCertificateName = properties.getProperty("egrulCertificateName");
            egrulCertificatePassword = properties.getProperty("egrulCertificatePassword");
            egrulDownloadDirectory = baseDirectory + properties.getProperty("egrulDownloadDirectory");
            egrulExtractDirectory = baseDirectory + properties.getProperty("egrulExtractDirectory");
            egrulFoldeUrl = properties.getProperty("egrulFoldeUrl");
            jdbcUrl = properties.getProperty("jdbcUrl");
            startEgripJobQuery = properties.getProperty("startEgripJobQuery");
            startEgrulJobQuery = properties.getProperty("startEgrulJobQuery");
            datePattern = properties.getProperty("datePattern");
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    //@formatter:off
    public static void main(String[] args) {
        String startDate = args.length > 0 ? args[0] : getYesterdayDate();
        String datePattern = args.length > 1 ? args[1] : App.datePattern;
        try{
            logger.trace("try parse " + startDate);
            new SimpleDateFormat(datePattern).parse(startDate);
        }catch (Exception e){
            logger.trace("parse error "+ e);
            System.err.println("Default date format is " + App.datePattern);
            System.exit(1);
        }

        new App(
            new OracleConnection(
                jdbcUrl
            ),
            new FileDownloadManager(
                    "EGRIP",
                    egripDownloadDirectory,
                    new ZipExtractorImpl(
                            FileSystems.getDefault().getPath(egripExtractDirectory)
                    ),
                    new FnsClientImpl(
                            host,
                            new SSLContextBuilderImpl(egripCertificateName, egripCertificatePassword)
                    ),
                    new ZipLinksParser(
                            new DirLnksParser(egripFolderUrl,
                                    new UrlFinder(
                                            new ModifyDateMoreThanCheker(
                                                    startDate,
                                                    App.datePattern,
                                                    new HrefContainsChecker(
                                                            "dir",
                                                            new HrefContainsChecker("FULL", false)
                                                    )
                                            )
                                    )
                            ),
                            new UrlFinder(
                                    new HrefContainsChecker(".zip")
                            )
                    )
            ),
            new FileDownloadManager(
                    "EGRUL",
                    egrulDownloadDirectory,
                    new ZipExtractorImpl(
                            FileSystems.getDefault().getPath(egrulExtractDirectory)
                    ),
                    new FnsClientImpl(
                            host,
                            new SSLContextBuilderImpl(egrulCertificateName, egrulCertificatePassword)
                    ),
                    new ZipLinksParser(
                            new DirLnksParser(egrulFoldeUrl,
                                    new UrlFinder(
                                            new ModifyDateMoreThanCheker(
                                                    startDate,
                                                    "yyyy-MM-dd HH:mm:ss",
                                                    new HrefContainsChecker(
                                                            "dir",
                                                            new HrefContainsChecker("FULL", false)
                                                    )
                                            )
                                    )
                            ),
                            new UrlFinder(
                                    new HrefContainsChecker(".zip")
                            )
                    )
            )
        ).run();
    }
    //@formatter:on

    public static String getYesterdayDate() {
        Calendar calendar = GregorianCalendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DATE, -1);
        return new SimpleDateFormat(datePattern).format(calendar.getTime());
    }

    private final OracleConnection oracleConnection;
    private final FileDownloadManager[] downloadManagers;
    private final Thread[] downloadManagersThreads;

    public App(OracleConnection oracleConnection, FileDownloadManager... downloadManagers) {
        this.oracleConnection = oracleConnection;
        this.downloadManagers = downloadManagers;
        this.downloadManagersThreads = new Thread[downloadManagers.length];
    }

    private void run() {
        for (int i = 0; i < downloadManagers.length; i++) {
            FileDownloadManager downloadManager = downloadManagers[i];
            Thread thread = new Thread(downloadManager);
            thread.start();
            downloadManagersThreads[i] = thread;
        }
        for (int i = 0; i < downloadManagersThreads.length; i++) {
            try {
                downloadManagersThreads[i].join();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        oracleConnection.execute(startEgripJobQuery);
        oracleConnection.execute(startEgrulJobQuery);
    }

}
