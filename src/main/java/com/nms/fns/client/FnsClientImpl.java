package com.nms.fns.client;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class FnsClientImpl implements FnsClient {

    private final String host;
    private final SSLSocketFactory socketFactory;
    private final SSLContextBuilder sslContextBuilder;

    FnsClientImpl(String host, SSLContextBuilder sslContextBuilder) {
        this.host = host;
        socketFactory = sslContextBuilder.get().getSocketFactory();
        this.sslContextBuilder = sslContextBuilder;
        HttpsURLConnection.setDefaultHostnameVerifier((s, sslSession) -> true);
    }

    public InputStream retrieve(String egripFolderUri) {
        try {
            URL url = new URL(host + egripFolderUri);
            HttpsURLConnection urlConn = (HttpsURLConnection) url.openConnection();
            urlConn.setSSLSocketFactory(socketFactory);
            return urlConn.getInputStream();
        }
        catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public FnsClient clone() {
        return new FnsClientImpl(this.host, this.sslContextBuilder);
    }
}
