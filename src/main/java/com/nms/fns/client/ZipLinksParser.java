package com.nms.fns.client;

import org.apache.log4j.Logger;

import java.io.InputStream;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

class ZipLinksParser {

    private final DirLnksParser dirParser;
    private final UrlFinder urlFinder;
    private final Logger logger = Logger.getLogger(ZipLinksParser.class);

    ZipLinksParser(DirLnksParser dirParser, UrlFinder urlFinder) {
        this.dirParser = dirParser;
        this.urlFinder = urlFinder;
    }

    Map<String, List<String>> parseZipLinks(FnsClient fnsClient) {
        Map<String, List<String>> result = new LinkedHashMap<>();
        List<String> dirs = dirParser.getDirs(fnsClient);
        for (String dirUri : dirs) {
            try (InputStream retrieve = fnsClient.retrieve(dirUri)) {
                List<String> zipLinks = urlFinder.parseUrls(retrieve);
                if (!zipLinks.isEmpty()) {
                    result.put(dirUri, zipLinks);
                }
            }
            catch (Exception e) {
                logger.error("Error zip fetch");
            }
        }
        return result;
    }
}
