package com.nms.fns.client;

import javax.net.ssl.SSLContext;

public interface SSLContextBuilder {
    SSLContext get();
}
